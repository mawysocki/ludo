public class Turn {

    public static Turn turn;
    private Player[] players;

    private int turnNumber = 1;

    private Turn(int noOfPlayers) {
        setPlayers(noOfPlayers);
    }

    public static void initPlayers(int playersNumber) {
        if (turn == null) {
            turn = new Turn(playersNumber);
        }

    }

    private void setPlayers(int playersNumber) {
        players = new Player[playersNumber];
        System.arraycopy(Player.values(), 0, players, 0, playersNumber);
    }

    public static Player getCurrentPlayer() {
        return turn.players[turn.turnNumber % turn.players.length];
    }

    public static void changeTurn() {
        turn.turnNumber++;
    }

    public static void resetTurn() {
        turn.turnNumber = 1;
    }
}
