import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FieldListener implements ActionListener {

    private final BoardGame boardGame;

    public FieldListener(BoardGame boardGame) {
        this.boardGame = boardGame;
    }
    @Override
    public void actionPerformed(ActionEvent e) {

        Field field = (Field)e.getSource();
        System.out.println("Owner: " + field.getOwner());
        if (isCorrectPlayer(field.getOwner())) {
            int newIndex = (field.getIndex()+Cube.value) % BoardGame.numberOfFields;
            boardGame.setOwner(newIndex, Turn.getCurrentPlayer());
            field.setOwner(null);
            boardGame.setFieldBackground(newIndex);
            boardGame.setFieldBackground(field, null);
            boardGame.updateActionLabel(ActionHints.THROW);
            boardGame.getCube().setEnabled(true);
            boardGame.setActiveFields(false);
//            goAround(field);
            Turn.changeTurn();
            boardGame.updateTurnLabel();
        } else {
            System.out.println("This filed is incorrect!");
        }



        //For testing purpose
//        if (field.getIndex() == 0)
//            boardGame.endGame();
    }

    private boolean isCorrectPlayer(Player p) {
        return Turn.getCurrentPlayer().equals(p);
    }
    //For testing purpose
    private void goAround(Field field) {
        Field neighbour = field.getNeighbour();
        if (neighbour.getOwner() == null) {
            neighbour.setOwner(Turn.getCurrentPlayer());
            neighbour.setBackground(Turn.getCurrentPlayer().color);
            goAround(neighbour);
        }
        else {
            System.out.println("index: " + neighbour.getIndex());
        }

    }

}
