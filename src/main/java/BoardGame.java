import javax.swing.*;
import java.awt.*;

public class BoardGame extends JPanel {

    private final Field[] fields;
    private final Game game;
    private final Cube cube;
    private final JButton startGame;
    private final JLabel currentActionLabel;
    private final JLabel currentTurnLabel;
    public final static int numberOfFields = 40;

    public BoardGame() {
        BoardFactory factory = new BoardFactory(this);
        setLayout(null);
        fields = factory.createFields();
        cube = factory.createCube();
        startGame = factory.createStartGameButton();
        currentActionLabel = factory.createActionLabel();
        currentTurnLabel = factory.createTurnLabel();
        setActiveFields(false);
        game = new Game(this);
    }

    public Cube getCube() {
        return cube;
    }

    public void setActiveFields(boolean isActive) {
        for (Field field : fields) {
            field.setEnabled(isActive);
        }
    }

    public void setFieldBackground(int index) {
        fields[index].setBackground(Turn.getCurrentPlayer().color);
    }

    public void setFieldBackground(Field field) {
        setFieldBackground(field, Turn.getCurrentPlayer().color);
    }

    public void setFieldBackground(Field field, Color color) {
        field.setBackground(color);
    }

    public void setOwner(int index, Player p) {
        fields[index].setOwner(p);
    }

    private void updateLabel(JLabel label, String action) {
        label.setText(action);
    }

    public void updateActionLabel(String action) {
        updateLabel(currentActionLabel, action);
    }

    public void updateTurnLabel() {
        String input = "Tura: " + Turn.getCurrentPlayer().toString();
        updateLabel(currentTurnLabel, input);
    }

    public void endGame() {
        updateActionLabel(ActionHints.RESET);
        cube.setEnabled(false);
        startGame.setEnabled(true);
        Turn.resetTurn();
    }
}
