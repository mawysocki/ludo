import java.awt.*;

public class FieldVectors {
    private final Point[] vectors;
    public FieldVectors(int size) {
        vectors = new Point[size];
        setVectors();
    }

    private void setVectors() {
        vectors[0] = new Point(0,0);
        vectors[1] = new Point(1,0);
        vectors[2] = new Point(1,0);
        vectors[3] = new Point(1,0);
        vectors[4] = new Point(1,0);
        vectors[5] = new Point(1,0);
        vectors[6] = new Point(0,-1);
        vectors[7] = new Point(0,-1);
        vectors[8] = new Point(0,-1);
        vectors[9] = new Point(1,0);
        vectors[10] = new Point(1,0);
        vectors[11] = new Point(0,1);
        vectors[12] = new Point(0,1);
        vectors[13] = new Point(0,1);
        vectors[14] = new Point(1,0);
        vectors[15] = new Point(1,0);
        vectors[15] = new Point(1,0);
        vectors[16] = new Point(1,0);
        vectors[17] = new Point(1,0);
        vectors[18] = new Point(1,0);
        vectors[19] = new Point(0,1);
        vectors[20] = new Point(0,1);
        vectors[21] = new Point(-1,0);
        vectors[22] = new Point(-1,0);
        vectors[23] = new Point(-1,0);
        vectors[24] = new Point(-1,0);
        vectors[25] = new Point(-1,0);
        vectors[26] = new Point(0,1);
        vectors[27] = new Point(0,1);
        vectors[28] = new Point(0,1);
        vectors[29] = new Point(-1,0);
        vectors[30] = new Point(-1,0);
        vectors[31] = new Point(0,-1);
        vectors[32] = new Point(0,-1);
        vectors[33] = new Point(0,-1);
        vectors[34] = new Point(-1,0);
        vectors[35] = new Point(-1,0);
        vectors[36] = new Point(-1,0);
        vectors[37] = new Point(-1,0);
        vectors[38] = new Point(-1,0);
        vectors[39] = new Point(0,-1);
    }

    public Point getVector(int index) {
        return vectors[index];
    }

}
