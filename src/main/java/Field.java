import javax.swing.*;
import java.awt.*;

public class Field extends JButton {
    public static final Dimension defaultSize = new Dimension(100, 100);
    private final BoardGame board;
    private final int index;
    private Field neighbour;
    private Player owner;
//    private boolean isSelected = false;



    public Field(int index, BoardGame board) {
        this.index = index;
        this.board = board;
        this.setSize(defaultSize);
        this.addActionListener(new FieldListener(board));
    }


    public int getIndex() {
        return index;
    }

    public Field getNeighbour() {
        return neighbour;
    }
    public void setNeighbour(Field neighbour) {
        this.neighbour = neighbour;
    }

    public void setOwner(Player player) {
        owner = player;
    }
    public Player getOwner() {
        return owner;
    }
}
