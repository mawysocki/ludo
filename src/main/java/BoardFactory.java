import javax.swing.*;
import java.awt.*;

public record BoardFactory(BoardGame boardGame) {

    public Field[] createFields() {
        Field[] fields = new Field[BoardGame.numberOfFields];
        for (int i = 0; i < BoardGame.numberOfFields; i++) {
            fields[i] = new Field(i, boardGame);
            boardGame.add(fields[i]);
            if (i == BoardGame.numberOfFields - 1) {
                fields[BoardGame.numberOfFields - 1].setNeighbour(fields[0]);
            }
            if (i > 0) {
                fields[i - 1].setNeighbour(fields[i]);
            }
        }
        setFieldsLocation(fields);
        mockUpFields(fields); //Only for test
        return fields;
    }

    private void setFieldsLocation(Field[] fields) {
        FieldVectors vectors = new FieldVectors(BoardGame.numberOfFields);
        fields[0].setLocation(new Point(0, Field.defaultSize.height * 3));
        for (int index = 1; index < fields.length; index++) {
            fields[index].setLocation(fields[index - 1].getLocation().x + Field.defaultSize.width * vectors.getVector(index).x, fields[index - 1].getLocation().y + Field.defaultSize.height * vectors.getVector(index).y);
        }
    }

    public Cube createCube() {
        Cube cube = new Cube(boardGame);
        boardGame.add(cube);
        return cube;
    }

    public JButton createStartGameButton() {
        JButton startGame = new JButton("Start game");
        startGame.setSize(Field.defaultSize.width * 2, Field.defaultSize.height);
        startGame.setLocation(Field.defaultSize.width * 2, 0);
        startGame.setFont(new Font("SansSerif", Font.BOLD, 30));
        startGame.addActionListener(e -> {
                    ((JButton) e.getSource()).setEnabled(false);
                    boardGame.getCube().setEnabled(true);
                    boardGame.updateActionLabel(ActionHints.THROW);
                }
        );
        boardGame.add(startGame);
        return startGame;
    }

    public JLabel createActionLabel() {
        JLabel currentActionLabel = new JLabel(ActionHints.START);
        currentActionLabel.setFont(new Font("SansSerif", Font.BOLD, 30));
        currentActionLabel.setSize(Field.defaultSize.width * 4, Field.defaultSize.height);
        currentActionLabel.setLocation(0, Field.defaultSize.height);
        boardGame.add(currentActionLabel);
        return currentActionLabel;
    }

    public JLabel createTurnLabel() {
        JLabel currentTurnLabel = new JLabel("Turn: " + Turn.getCurrentPlayer());
        currentTurnLabel.setFont(new Font("SansSerif", Font.BOLD, 30));
        currentTurnLabel.setSize(Field.defaultSize.width * 4, Field.defaultSize.height);
        currentTurnLabel.setLocation(0, Field.defaultSize.height + 40);
        boardGame.add(currentTurnLabel);
        return currentTurnLabel;
    }

    //For testing purpose
    private void mockUpFields(Field[] fields) {
        fields[0].setOwner(Player.GREEN);
        fields[0].setBackground(Player.GREEN.color);
        fields[1].setOwner(Player.RED);
        fields[1].setBackground(Player.RED.color);
        fields[2].setOwner(Player.GREEN);
        fields[2].setBackground(Player.GREEN.color);
        fields[3].setOwner(Player.RED);
        fields[3].setBackground(Player.RED.color);
    }


}
