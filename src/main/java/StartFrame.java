import javax.swing.*;
import java.awt.*;

public class StartFrame extends JFrame {

    private final JPanel panel;
    private JComboBox<Integer> noOfPlayersBox;

    public StartFrame() {
        panel = new JPanel();
        setupFrame();
    }

    private void setupFrame() {
        this.setSize(new Dimension(300, 300));
        this.add(panel);
        addLabel();
        addComboBox();
        addStartButton();
        this.centerWindow();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }



    private void centerWindow() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = screen.width / 2 - this.getSize().width / 2;
        int y = screen.height / 2 - this.getSize().height / 2;
        this.setLocation(x, y);
    }

    private void addLabel() {
        JLabel infoLabel = new JLabel("Select number of players");
        panel.add(infoLabel);
    }
    private void addComboBox() {
        Integer[] options = {2,3,4};
        noOfPlayersBox = new JComboBox<>(options);
        panel.add(noOfPlayersBox);
    }
    private void addStartButton() {
        JButton startButton = new JButton("START");
        startButton.addActionListener(e -> initGame());
        panel.add(startButton);
    }

    private void initGame() {
        String comboBoxValue = noOfPlayersBox.getSelectedItem().toString();
        Turn.initPlayers(Integer.parseInt(comboBoxValue));
        System.out.println("Selected players: " + comboBoxValue);
        new BoardFrame();
        this.dispose();
    }
}
