import java.awt.*;

public enum Player {

    RED(Color.RED), GREEN(Color.GREEN), BLUE(Color.BLUE),  YELLOW(Color.YELLOW);

    public final Color color;
    Player(Color color) {
        this.color = color;
    }
}
