import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Cube extends JButton {
    Random random;
    BoardGame board;
    public static int value = 0;

    public Cube(BoardGame board) {
        this.board = board;
        random = new Random();
        initCube();
        setupListener();

    }

    private void initCube() {
        this.setSize(Field.defaultSize.width, Field.defaultSize.height);
        this.setLocation(new Point(0, 0));
        this.setBackground(Color.BLACK);
        this.setForeground(Color.WHITE);
        this.setFont(new Font("SansSerif", Font.BOLD, 50));
        this.setText("0");
        this.setEnabled(false);
    }

    private void setupListener() {
        this.addActionListener(e -> new Thread(new MyRunnable(board)).start());
    }

    public void throwCube() {
        value = 0;
        for (int i = 0; i < 10; i++) {
            value = random.nextInt(6) + 1;
            this.setText("" + value);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
