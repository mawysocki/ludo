import javax.swing.*;
import java.awt.*;

public class BoardFrame extends JFrame {

    public final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    private final BoardGame panel;
    public BoardFrame() {
        panel = new BoardGame();
        setupFrame();
    }

    private void setupFrame() {
        this.setSize(SCREEN_SIZE);
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
