public class MyRunnable implements Runnable{

    private final BoardGame board;
    private final Cube cube;
    public MyRunnable(BoardGame board) {
        this.board = board;
        this.cube = board.getCube();
    }
    @Override
    public void run() {
        cube.throwCube();
        board.updateActionLabel(ActionHints.SELECT);
        cube.setEnabled(false);
        board.setActiveFields(true);
    }
}
